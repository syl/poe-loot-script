[screenshot](https://i.imgur.com/N1J9vLM.png)

This filter keeps the text color of the base item, it is only altered when it's ultra rare (Exalted Orb).

##### Border color

- red:
    * ultra rare items (fishing rod)
    * rare uniques (shav)
    * rare currencies (exalted)
    * gem quality 15+
    * 5+ links
- purple:
    * rare ilevel 75+, for regal recipe
    * normal rings and amulets 75+ (craft)
- yellow:
    * unique items
    * rare items ilevel 60+, for chaos recipe
    * gem quality 10+
    * jewels
- others:
    * light blue: items to vendor (chroma, 6 sockets, magic rings)
    * pink: items with quality to vendor (chisel)
    * grey: utility flask
    * gem color: gem with quality
    * light green text: normal item to chance
